"""Analysis tools for ntuples"""

import os
from math import sqrt, tan, atan, exp, cos, sin
from ROOT import TFile, TAxis, TCanvas, gStyle, TH2F, TH1F, gPad, gDirectory
import collections, re


def create_output_dir(outDir):
    """Create output directory if it does not exist yet"""
    if not os.path.exists(outDir):
        os.makedirs(outDir)


def save_histograms(histDict, canvas, outDir, imgType, logScale=False, doFit=False):
    """Save generated histograms as ROOT file and image files, optionally fit Gaussian"""

    outFileName = "{}.root".format(outDir)
    outFile = TFile(outFileName, "recreate")

    gStyle.SetOptTitle(0)
    gStyle.SetPadTopMargin(0.05)
    gStyle.SetPadBottomMargin(0.13)
    gStyle.SetPadLeftMargin(0.16)
    gStyle.SetPadRightMargin(0.02)

    logString = ""
    if logScale:
        canvas.SetLogy(True)
        logString = "_log"

    for key, item in histDict.items():
        # do not save empty histograms
        if (type(item) == TH1F) or (type(item) == TH2F):
            if item.GetEntries() == 0:
                continue

        # write histogram to file
        # if type(item) == TH2F or type(item) == TH1F:
        #   item.Sumw2()

        item.Write()

        if type(item) == TH2F:
            gStyle.SetOptStat(0)
            item.Draw("colz")
            item.GetYaxis().SetTitleOffset(1.5)
        else:
            gStyle.SetOptStat("mr")
            if type(item) == TH1F:
                item.Draw("hist")
                item.GetYaxis().SetTitleOffset(1.5)
                canvas.SetGrid()
                # move the stats box
                gPad.Update()
                ps = item.FindObject("stats")
                ps.SetX1NDC(0.15)
                ps.SetX2NDC(0.35)
                canvas.Modified()
                canvas.Update()
            else:
                item.Draw()

        # if doFit:
        #     if key.find("radius_in_layer") > 0:
        #         gStyle.SetOptFit(1)
        #         fit = item.Fit("gaus")
                #sigma = fit.getParameter(1)
                #canvas.SaveAs("{}/{}{}_projectionX.{}".format(outDir, key, logString, imgType))

        if item.GetYaxis().GetTitle() == "":
            item.GetYaxis().SetTitle("a.u.")

        canvas.SaveAs("{}/{}{}.{}".format(outDir, key, logString, imgType))

        if type(item) == TH2F:
            gStyle.SetOptStat("mr")
            pjX = item.ProjectionX("pjX")
            pjX.Draw()
            canvas.SaveAs("{}/{}{}_projectionX.{}".format(outDir, key, logString, imgType))
            pjX.Delete()
            pjY = item.ProjectionY("pjY")
            pjY.Draw()
            canvas.SaveAs("{}/{}{}_projectionY.{}".format(outDir, key, logString, imgType))
            pjY.Delete()
            pfX = item.ProfileX("pfX")
            pfX.Draw()
            canvas.SaveAs("{}/{}{}_profileX.{}".format(outDir, key, logString, imgType))
            pfX.Delete()
            pfY = item.ProfileY("pfY")
            pfY.Draw()
            canvas.SaveAs("{}/{}{}_profileY.{}".format(outDir, key, logString, imgType))
            pfY.Delete()

    outFile.Write()
    outFile.Close()


def radius_per_layer(outDir, histDict, energy_tot):
    """Find 68% and 90% CL for each layers"""
    

    # work in one dimension
    list_layer = []
    list_rho_1s = []
    list_rho_2s = []
    list_content1 = []
    list_content2 = []

    integrale = 0

    # Take the layer ID from the name of the file
    d = dict((k, v) for k, v in histDict.items() if k.startswith('RecHits_radius_in_layer'))
    for key, item in d.items():
        layerID = key[-2:]
        layer = str(layerID)
        if 'r' in layer:
            layer = layer[-1:]

        list_layer.append(layer)

        # For each layer deltaR in 1 and 2 CL
        max_bin_id = item.GetBin(item.GetMaximumBin())
        comp = 0
        content = 0

        integrale += item.Integral()

        while content <= 0.68*item.Integral():
            if comp == 0:
                content = item.GetMaximum()
            else:
                if max_bin_id-comp < 0:
                    content += item.GetBinContent(max_bin_id+comp)
                else:
                    content += item.GetBinContent(max_bin_id-comp) + item.GetBinContent(max_bin_id+comp)
            comp = comp + 1

        list_content1.append(content*100/energy_tot)
        print('percentage energy ; {}'.format(list_content1))

        bin_1CL_top_Limit = item.GetBinCenter(max_bin_id+(comp-1))
        bin_1CL_low_Limit = item.GetBinCenter(max_bin_id-(comp-1))
        radius_1CL = abs((bin_1CL_top_Limit - bin_1CL_low_Limit)/2)
        list_rho_1s.append(radius_1CL)

        while content <= 0.90*item.Integral():
            content += item.GetBinContent(max_bin_id-comp) + item.GetBinContent(max_bin_id+comp)
            comp = comp + 1

        list_content2.append(content*100/energy_tot)
        print('percentage energy ; {}'.format(list_content2))


        bin_2CL_top_Limit = item.GetXaxis().GetBinCenter(max_bin_id+(comp-1))
        bin_2CL_low_Limit = item.GetXaxis().GetBinCenter(max_bin_id-(comp-1))
        radius_2CL = abs((bin_2CL_top_Limit - bin_2CL_low_Limit)/2)
        list_rho_2s.append(radius_2CL)

    rho1 = dict(zip(list_layer, zip(list_rho_1s, list_content1)))
    for k in sorted(rho1, key=int):
        # print('layer : {}, radius : {}, content : {}'.format(k,rho1[k][0], rho1[k][1]))
        histDict["RecHits_radius_layers"].Fill(k,rho1[k][0],rho1[k][1])

    # Same for 2sigma
    rho2 = dict(zip(list_layer, zip(list_rho_2s, list_content2)))
    for k in sorted(rho2, key=int):
        histDict["RecHits_radius_layers"].Fill(k, rho2[k][0], rho2[k][1])

    print('Integrale 2D plot : {}'.format(integrale))


    # Other approach, compute direclty from the 2D plot
    # # Take information from the shower 2D histogram
    # cHist = TCanvas("cHist", "Canvas 1", 100, 100, 800, 800)
    # histo = TH2F("histo_shower", "histo_layers_deltaR;layers;#rho [mm]", 30, 0, 30, 500, 0, 500)
    # # histo = [histogram for name, histogram in histDict.items() if name.startswith('RecHits_shower')]

    # filename = outDir + '.root'
    # cHist.cd()
    # myfile = TFile.Open(filename)
    # histo = myfile.RecHits_shower.Clone()
    # histo.SetDirectory(0)
    # cHist.Print('./histo.pdf')

    # for layer in range(1, histo.GetXaxis().GetNbins()):
    #     # bx = histo.GetXaxis().GetBinCenter(layer)

    #     content_max = 0
    #     bin_max_ID = 0
    #     cu = 0
    #     bin = 0

    #     compt = 0
    #     content = 0

    #     radius_1CL = 0
    #     radius_2CL = 0

    #     print('Layer : {}'.format(layer))

    #     for radius in range(0, histo.GetYaxis().GetNbins()):
    #         y_integrale = 0
    #         # by = histo.GetYaxis().GetBinCenter(radius)
        
    #         bin = histo.GetBin(layer, radius)
    #         cu = histo.GetBinContent(bin)

    #         y_integrale = histo.Integral(layer, layer, 0, 500)
    #         # Get the maximal value and its binID
    #         if content_max < cu:
    #             content_max = cu
    #             bin_max_ID = bin

    #         for compt in range(0, 15):
    #             print('bin_max : {}'.format(bin_max_ID))

    #             if compt == 0:
    #                 print('content = {}'.format(content_max))
    #             else:
    #                 print('bin_max_ID+(compt*30)+1) : {}'.format(bin_max_ID+(compt*30)+1))
    #                 print('bin_max_ID-(compt*30)+1) : {}'.format(bin_max_ID-(compt*30)+1))

    #                 print('content bin + 1= {}'.format(histo.GetBinContent(bin_max_ID+(compt*30)+1)))
    #                 print('content bin - 1= {}'.format(histo.GetBinContent(bin_max_ID-(compt*30)+1)))
    #             # print('Test : bin y = {}, bin x = {}, binID = {}, content = {}'.format(by, bx, bin, cu))
    #     print('Bin max : {}, bin max position : {}'.format(content_max, bin_max_ID))
    #     print('Integrale : {}'.format(y_integrale))

    # ========> Works until here. Then, the while loop for layer 7 never stop.
    # I think there is bad bin numerotation and then bad content computation

        # # Find the 1sigma radius  
        # while content < 0.68*y_integrale:
        #     if compt == 0:
        #         content += content_max
        #     else:
        #         if (bin_max_ID-(compt*30)+1 < 0):
        #             content += histo.GetBinContent(bin_max_ID+(compt*30)+1)
        #         else:
        #             content += histo.GetBinContent(bin_max_ID-(compt*30)+1) + histo.GetBinContent(bin_max_ID+(compt*30)+1)
        #     print('content : {}'.format(content))
        #     compt = compt + 1
        # print('compt = {}, content : {}'.format(compt, content))

        # bin_1CL_top_Limit = histo.GetYaxis().GetBinCenter(bin+(compt-1)*30+1)
        # bin_1CL_low_Limit = histo.GetYaxis().GetBinCenter(bin_max_ID)
        # radius_1CL =  bin_1CL_top_Limit - bin_1CL_low_Limit
        # print('Radius : {}'.format(radius_1CL))

    #     # Find the 2sigma radius
    #     while content <= 0.90*y_integrale:
    #         if bin-compt*30+1 < 0:
    #             content += histo.GetBinContent(bin+compt*30+1)
    #         else :
    #             content += histo.GetBinContent(bin-compt*30+1) + histo.GetBinContent(bin+compt*30+1)
    #         compt = compt + 1
    #     print('Content : {}, compteur : {}'.format(content, compt))

    #     radius_2CL = histo.GetYaxis().GetBinCenter(bin+compt*30+1)

        # print('Radius 1Cl = {}'.format(radius_1CL))
        # print('Radius 2Cl = {}'.format(radius_2CL))

        # histDict["RecHits_radius_layers"].Fill(layer, radius_1CL)
        # histDict["RecHits_radius_layers"].Fill(layer, radius_2CL)


def get_collection_Ecut(collection, event, histDict, Ecut):
    """select collection based on Ecut and save in histogram."""
    Collection = []
    if collection == "e.particles":
        collectionType = "Part"
    else:
        collectionType = "SimClus"

    for cIndex, c in enumerate(collection):
        # histDict["{}_energy".format(collectionType)].Fill(c.energy)
        # histDict["{}_pt".format(collectionType)].Fill(c.pt)
        # histDict["{}_eta".format(collectionType)].Fill(c.eta)
        # histDict["{}_phi".format(collectionType)].Fill(c.phi)

        if c.energy == Ecut:
            Collection.append(c.Clone())
            # histDict["{}_energy_pass".format(collectionType)].Fill(c.energy)
            # histDict["{}_pt_pass".format(collectionType)].Fill(c.pt)
            # histDict["{}_eta_pass".format(collectionType)].Fill(c.eta)
            # histDict["{}_phi_pass".format(collectionType)].Fill(c.phi)

    return Collection


def deltaR(sc, p):
    deta = sc.eta - p.eta
    dphi = sc.phi - p.phi
    return sqrt(deta*deta + dphi*dphi)

class Geometry(object):
    """Sample class to get ROOT Chain and individual files."""

    def __init__(self):
        """initialise directories."""
        self.z_abs = {}
        self.z_rel = {}
        self.x0_cumulative = {}
        self.x0 = {}
        self.dEdx_cumulative = {}
        self.dEdx = {}
        self.lamda_cumulative = {}
        self.lamda = {}

    def addLayer(self, layerGeo):
        """add geometry for a layer following structure in file."""
        layer = int(layerGeo[0])+1
        if layer in self.z_abs:
            raise RuntimeError
        self.z_abs[layer] = float(layerGeo[1])
        self.z_rel[layer] = float(layerGeo[2])
        self.x0_cumulative[layer] = float(layerGeo[3])
        self.x0[layer] = float(layerGeo[4])
        self.dEdx_cumulative[layer] = float(layerGeo[5])
        self.dEdx[layer] = float(layerGeo[6])
        self.lamda_cumulative[layer] = float(layerGeo[7])
        self.lamda[layer] = float(layerGeo[8])

    def layerToZ(self, layer, eta):
        """convert layer with eta-information to z value."""
        z_abs = self.z_abs[layer]
        if (eta < 0):
            z_abs *= -1.
        return z_abs

    def layerEtaPhiToX(self, layer, eta, phi):
        """return absolute X value."""
        z = self.layerToZ(layer, eta)
        t = exp(-1. * eta)
        if (t == 1):
            x = 0
        else:
            x = z * 2 * t * cos(phi)/(1 - t*t)
        return x

    def layerEtaPhiToY(self, layer, eta, phi):
        """return absolute Y value."""
        z = self.layerToZ(layer, eta)
        t = exp(-1. * eta)
        if (t == 1):
            y = 0
        else:
            y = z * 2 * t * sin(phi)/(1 - t*t)
        return y


def parseGeometry(geoFilename):
    """use regular expressions to parse geometry file."""
    import re
    fpg = "([0-9]*\.?[0-9]+)"  # floating point group
    regex = ur"S[a-z]\s*(\d+): z=\({}\)\s*({})\s*mm;\s*X0=\(\s*{}\)\s*{}; dEdx=\(\s*{}\)\s*{};\s*l=\(\s*{}\)\s*{}".replace("{}", fpg)

    geometry = Geometry()
    with open(geoFilename) as f:
        content = f.readlines()
        for line in content:
            matches = re.finditer(regex, line)
            for matchNum, match in enumerate(matches):
                geometry.addLayer(match.groups())

    return geometry


def main():
    """Main function"""
    print "not implemented"

if __name__ == '__main__':
    main()
