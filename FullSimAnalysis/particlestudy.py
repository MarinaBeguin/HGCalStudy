import numpy as np
import logging
from ROOT import TChain, TH1F, TH2F, TCanvas, TLorentzVector
from math import sqrt 

import ntupltools



def get_hits(list_p, list_sc, dRCut, histDict):
    max_index = min(len(list_p), len(list_sc))
    hits = []

    for i in range(max_index):
        p = list_p[i]
        sc = list_sc[i]

        delta = ntupltools.deltaR(sc, p)
        if delta < dRCut:
            hits.extend(sc.hits)

    return hits


def getHitList(hits_list, rechits_detid_list):
    """Get hits list indices from recHitDetIds."""
    hits_array = np.array(hits_list)
    rechits_detid_array = np.array(rechits_detid_list)

    return np.nonzero(np.in1d(rechits_detid_array, hits_array))


def get_files_to_process(ntupllist):
    with open(ntupllist) as f:
        lines = f.readlines()

    lines = [line.replace('\n', '') for line in lines]

    valid_lines = [line for line in lines
                   if line.endswith('.root') and not line.startswith('#')]

    return valid_lines

def getHists():
    """function to book all histograms and return as dictionary"""
    histDict = {}
    # histDict["selectedEvents"] = TH1F("selectedEvents", "selectedEvents", 1, 0.5, 1.5)

    cluster = ["SimClus", "Part", "RecHits"]
    for clus in cluster :
        histDict["{}_energy".format(clus)] = TH1F("{}_energy".format(clus), "{}_energy;E[GeV]".format(clus), 100, 0, 40)
        histDict["{}_pt".format(clus)] = TH1F("{}_pt".format(clus), "{}_pt;pT[GeV]".format(clus), 100, 0, 12)
        histDict["{}_eta".format(clus)] = TH1F("{}_eta".format(clus), "{}_eta;#eta".format(clus), 100, -4, 4)
        histDict["{}_phi".format(clus)] = TH1F("{}_phi".format(clus), "{}_phi;#phi".format(clus), 100, -3.2, 3.2)

        histDict["{}_energy_pass".format(clus)] = TH1F("{}_energy_pass".format(clus), "{}_energy_pass;E [GeV]".format(clus), 100, 0, 40)
        histDict["{}_pt_pass".format(clus)] = TH1F("{}_pt_pass".format(clus), "{}_pt_pass;pT [GeV]".format(clus), 100, 0, 12)
        histDict["{}_eta_pass".format(clus)] = TH1F("{}_eta_pass".format(clus), "{}_eta_pass;#eta".format(clus), 100, -4, 4)
        histDict["{}_phi_pass".format(clus)] = TH1F("{}_phi_pass".format(clus), "{}_phi_pass;#phi".format(clus), 100, -3.2, 3.2)

        if clus == "RecHits":
            histDict["{}_x_pass".format(clus)] = TH1F("{}_x_pass".format(clus), "{}_x_pass;x[cm]".format(clus), 200, -150, 150)
            histDict["{}_y_pass".format(clus)] = TH1F("{}_y_pass".format(clus), "{}_y_pass;y[cm]".format(clus), 100, -30, 30)
            histDict["{}_z_pass".format(clus)] = TH1F("{}_z_pass".format(clus), "{}_z_pass;z[cm]".format(clus), 100, 320, 520)
            histDict["{}_layer_energy".format(clus)] = TH2F("{}_layer_energy".format(clus), "{}_layer_energy;layers;energy [GeV]".format(clus), 30, 0, 30, 100, 0, 4)

            histDict["{}_deltaX_layers".format(clus)] = TH2F("{}_deltaX_layers".format(clus), "{}_deltaX_layers;layers;#Delta x [mm]".format(clus), 30, 0, 30, 100, -500, 500)
            histDict["{}_deltaY_layers".format(clus)] = TH2F("{}_deltaY_layers".format(clus), "{}_deltaY_layers;layers;#Delta y [mm]".format(clus), 30, 0, 30, 100, -500, 500)
            histDict["{}_lateral_shower_width_X".format(clus)] = TH1F("{}_lateral_shower_width_X".format(clus), "{}_lateral_shower_width_X;#Delta x [mm]".format(clus),100, -500, 500) 
            histDict["{}_lateral_shower_width_Y".format(clus)] = TH1F("{}_lateral_shower_width_Y".format(clus), "{}_lateral_shower_width_Y;#Delta y [mm]".format(clus),100, -500, 500) 

            histDict["{}_shower".format(clus)] = TH2F("{}_shower".format(clus), "{}_layers_deltaR;layers;#rho [mm]".format(clus), 29, 1, 30, 500, 0, 500)
            histDict["{}_deltaPhi_deltaEta".format(clus)] = TH2F("{}_deltaPhi_deltaEta".format(clus), "{}_deltaPhi_deltaEta;#Delta #eta; #Delta #phi".format(clus), 100, -4, 4, 100, -1, 7)

            # transversal shower profile : one histogram per layer for fit
            for layer in range(1, 29):
                histDict["{}_radius_in_layer{}".format(clus, layer)] = TH1F("{}_radius_in_layer{}".format(clus, layer), "{}_radius_in_layer{};#rho [mm]".format(clus, layer), 500, 0, 500)

            histDict["{}_radius_layers".format(clus)] = TH2F("{}_radius_layer".format(clus), "{}_radius_layer;layer number;#rho [mm]".format(clus), 30, 0, 30, 180, 0, 180)
    return histDict


def event_process(histDict, chain, maxLayer, Ecut, geometry):

    sampleEvents = chain.GetEntries()
    print("Events : {}".format(sampleEvents))

    sum_energy = 0
    # Start event loop
    for event, e in enumerate(chain):

        if event%100 == 0:
            print('Processing event {} of {}'.format(event, sampleEvents))

        # get the rechits_raw detid list
        rechits_detid = []
        rechits = e.rechits_raw
        rechits_detid = [rhit.detid for rhit in rechits]
        # histDict["RecHits_energy"].Fill(rhit.energy)
        # histDict["RecHits_pt"].Fill(rhit.pt)
        # histDict["RecHits_eta"].Fill(rhit.eta)
        # histDict["RecHits_phi"].Fill(rhit.phi)

        # get particles with E = 35 GeV (only electrons)
        selected_sc = ntupltools.get_collection_Ecut(e.simcluster, e, histDict, Ecut)
        selected_part = ntupltools.get_collection_Ecut(e.particles, e, histDict, Ecut)

        # Get the simCluster hits close to particles (deltaR < 0.2)
        hits = []
        hits = get_hits(selected_part, selected_sc, 0.2, histDict)

        # Association of selected simCluster hits with the corresponding rechits_raw
        simClusHitAssoc = []
        simClusHitAssoc.append(getHitList(hits, rechits_detid))

        #Initialisation TLorentzVector per layer
        recHitVectorsLayer = {}
        for layer in range(1, maxLayer):
            recHitVectorsLayer[layer] = TLorentzVector()

        #use the selected rechits_raw from layer 1 to 28
        selected_rh = simClusHitAssoc[0][0]


        # loop over selected rechits
        for index, elem in enumerate(selected_rh):
            thisRh = rechits[elem]

            if thisRh.layer < maxLayer:
                #total shower energy per event
                sum_energy += thisRh.energy/1000

                # histDict["RecHits_energy_pass"].Fill(thisRh.energy)
                # histDict["RecHits_pt_pass"].Fill(thisRh.pt)
                # histDict["RecHits_eta_pass"].Fill(thisRh.eta)
                # histDict["RecHits_phi_pass"].Fill(thisRh.phi)
                # histDict["RecHits_x_pass"].Fill(thisRh.x)
                # histDict["RecHits_y_pass"].Fill(thisRh.y)
                # histDict["RecHits_z_pass"].Fill(thisRh.z)

                # weight : GeV/event
                eweight = thisRh.energy/1000
                # histDict["RecHits_layer_energy"].Fill(thisRh.layer, thisRh.energy, eweight)

                recHitTLV = TLorentzVector()
                recHitTLV.SetPtEtaPhiE(thisRh.pt, thisRh.eta, thisRh.phi, thisRh.energy)
                recHitVectorsLayer[thisRh.layer] += recHitTLV

                #loop over individual RecHits again to compare against energy-weighted RecHitCluster
                # first try to determine the layer where the rechits deposited the maximum energy
                layerEmax = 0
                Emax = 0
                for layer, tlv in recHitVectorsLayer.iteritems():
                    if tlv.E() > Emax:
                        Emax = tlv.E()
                        layerEmax = layer

                # now get x-y coordinates of that cluster
                maxRecHitsClus_x = geometry.layerEtaPhiToX(layerEmax, recHitVectorsLayer[layerEmax].Eta(), recHitVectorsLayer[layerEmax].Phi())
                maxRecHitsClus_y = geometry.layerEtaPhiToY(layerEmax, recHitVectorsLayer[layerEmax].Eta(), recHitVectorsLayer[layerEmax].Phi())

                # Cluster took as reference
                radiusReferenceObject = {}
                radiusReferenceObject["RecHitsClusMaxLayer"] = (maxRecHitsClus_x, maxRecHitsClus_y)

                # Calculation of radii
                for refKey, refObject in radiusReferenceObject.iteritems():
                    thisRh_x = geometry.layerEtaPhiToX(thisRh.layer, thisRh.eta, thisRh.phi)
                    thisRh_y = geometry.layerEtaPhiToY(thisRh.layer, thisRh.eta, thisRh.phi)

                    deltaX = thisRh_x-refObject[0]
                    deltaY = thisRh_y-refObject[1]
                    distanceSquared = sqrt(deltaX**2 + deltaY**2)

                    # Fill the shower behaviour in layers 
                    histDict["RecHits_shower"].Fill(thisRh.layer, distanceSquared, eweight*100/35)

                    # radius distribution in each layer, weight = energy per event
                    # Easier manipulation to find the 1sigma and 2sigma of transversal profile
                    histDict["RecHits_radius_in_layer{}".format(thisRh.layer)].Fill(distanceSquared, eweight)


                    # histDict["RecHits_lateral_shower_width_X"].Fill(deltaX, eweight)
                    # histDict["RecHits_lateral_shower_width_Y"].Fill(deltaY, eweight)
                    # histDict["RecHits_deltaX_layers"].Fill(thisRh.layer, deltaX, eweight)
                    # histDict["RecHits_deltaY_layers"].Fill(thisRh.layer, deltaY, eweight)

                # delta phi delta eta
                delta_phi = abs(recHitVectorsLayer[layerEmax].Phi() - thisRh.phi)
                delta_eta = abs(recHitVectorsLayer[layerEmax].Eta() - thisRh.eta)
                # histDict["RecHits_deltaPhi_deltaEta"].Fill(delta_eta, delta_phi)

    # print("sum  energy : {} GeV".format(sum_energy))



def main():
    """ Main function and settings """

    outDir = "electrons_nPart1_1000e_E35"
    ntupltools.create_output_dir(outDir)
    imgType = "pdf"
    canvas = TCanvas(outDir, outDir, 900, 700)

    histDict = getHists()

    geometry = ntupltools.parseGeometry('Geometry.txt')

    root_files = get_files_to_process('ntupllist.txt')
    tree_name = '/ana/hgc'
    chain = TChain('T')
    for root_file in root_files:
        chain.Add(root_file + tree_name)

    Ecut = 35.      #keep only non irradiated electrons
    maxLayer = 29   #Only EE

    event_process(histDict, chain, maxLayer, Ecut, geometry)
    # ntupltools.save_histograms(histDict, canvas, outDir, imgType, logScale=False, doFit=False)
    ntupltools.radius_per_layer(outDir, histDict, 35)
    ntupltools.save_histograms(histDict, canvas, outDir, imgType, logScale=False, doFit=False)


if __name__ == '__main__':
    main()
