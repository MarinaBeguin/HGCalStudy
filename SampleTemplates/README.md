These templates allow to generate and reconstruct the simulated events

To run 
==============

Build directory to contain templates and GSD, RECO and NTUP directories

    mkdir simulation_for_exemple
    cd simulation
    mkdir GSD RECO NTUP

Call the CMS environnement in this directory

    cmsenv

Run the templates in the following order :

    cmsRun name_of_template_GSD.py
    cmsRun name_of_template_RECO.py
    cmsRun name_of_template_NTUP.py

Each of this command takes several minutes to execute (2 hours for the GSD, 1
hour for RECO and 30 minutes for NTUP)


To configure these files
==============

Output
==============

